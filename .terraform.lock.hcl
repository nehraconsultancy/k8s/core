# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/aws" {
  version     = "5.68.0"
  constraints = "~> 5.0"
  hashes = [
    "h1:VMfgVqBZ6PPm6vIk0z1jHKX8SHK+/x4IfbOkZhaD6p4=",
    "zh:0501ccb379b74832366860699ca6d5993b164ec44314a054453877d39c384869",
    "zh:315b4eb957f84ce5580fed31e4b99b25d41634832a6939cd016fb0c4963164c9",
    "zh:31defa4c379a4f1761504617824bae1b5efc93f456f055f85d1131676433085d",
    "zh:3702a13f06369ee90eea413ec32db6ffa9c59648b3545301f9917f6774a840cb",
    "zh:7c524cb809267ec68dd67124aa8d9fbab7722814fa875b1306d527f71b8b3bea",
    "zh:ab37ec8b17be8062d804c17f5f4ddd9deaf50b3a48e6c0b979b60ef80f85192b",
    "zh:baaf2c46edfe596f085f0f8f389e908a874e45c42ea5e5d5f24de1dbfed7542e",
    "zh:cb37278073ede7b5e18116faebea49d5d47496d5093cec6c69065fb9ad1f622d",
    "zh:ec4b64d66470b078162c13479446ad6819c93099149b478f43d990702f937fd3",
    "zh:f55c3a3ba975ecfe73c729a085efb0432c02c74e91edaf40d351cdb231c3836b",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.0.3"
  constraints = "2.0.3"
  hashes = [
    "h1:Cm/+lMSGFiNunrIOJd1vxQHEeNslHLCV1u3YXK0woZA=",
    "zh:6169d7eaf5c4201499e681644303d4bd38e8053338a07e306173baf61db673cf",
    "zh:63745aa784fc6bdfa9af62d91019165b524303e0652496956bd4d5d169b4a27f",
    "zh:7092407ef6fdae54d68a435fe2adb24be9bb5a858b5a700314387d77887d89d9",
    "zh:8df6daf1d9a104053dc47f1a48a9c2da52768153f83f0cc2d5cf8c8a9107cbc9",
    "zh:8f5f9d997056222c9a07f59791fd7a0d12f48916f1147024f8fbc64dafefb815",
    "zh:ca182d241cee3a3fd1f89f0ade95b453133b5206e8606a75080def42d8b79eb9",
    "zh:cc538833eee6f1ba67e5349a8f266edc377bee3a86fccd6dd576dc6944716188",
    "zh:e9b37ee10e0d52c2966f21ee7eceda3ddc769f1b9b972e741e6f91c75566f2e4",
    "zh:f4ac62fd4ec74432687dbe74283dee2d5fd01e9298416540320f32b29ae59bbe",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.32.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:ZRCFOIecOlTIrpf1O/kmbFfBMQe9r8/HwiiK9kP0KEk=",
    "zh:06d586c8fcd3ab8fe7f3ac99142ba48b9efbff8bebe05c52b3c7997f83146200",
    "zh:12ce862493717118a6bf68328448d09023a60344da25633e124423cdd734263e",
    "zh:33ee1cda5db58fd26576ba6be715282af30e04d25b38fd6752810fd206bc6422",
    "zh:8f4e13c726a5fb84244eff7740b20678e7fb2d5df6ebc759101d4c58fb069112",
    "zh:8fe15d350b5a018f535a93fa054bf4d05377a69f3b1e5cabe8c73d059a4b70cb",
    "zh:953fc8c8a92ff0defafd22ee0aec12d483d7b80685de6838e513d4de7170a651",
    "zh:a1ad6197105f9cda73c39f3b69dd688ec22708c736de05c03516561a88f4bbfc",
    "zh:c1d60898c269f42ece0b3672901001ba26338c865f83a39b116c0d6c0cd8dbc1",
    "zh:d26fcff2fda9421d9129fd407696481ecd2714ae3316e81ff977e2e40de068e5",
    "zh:dc616b73095755245f211af0989bfcf2f76b43196bf7f8982183e4e3b1c3f6f6",
  ]
}
