resource "kubernetes_manifest" "lb_addresses" {
  depends_on = [helm_release.metallb]
  manifest = {
    apiVersion = "metallb.io/v1beta1"
    kind       = "IPAddressPool"
    metadata = {
      name      = "lb-addresses"
      namespace = "metallb-system"
    }
    spec = {
      addresses = ["192.168.101.224/27"]
    }
  }
}
resource "kubernetes_manifest" "l2_advertizement" {
  depends_on = [helm_release.metallb]
  manifest = {
    apiVersion = "metallb.io/v1beta1"
    kind       = "L2Advertisement"
    metadata = {
      name      = "lb-addresses"
      namespace = "metallb-system"
    }
  }
}
locals {
  ingresses = {
    default    = "192.168.101.240"
    media      = "192.168.101.225"
    monitoring = "192.168.101.226"
  }
}
resource "helm_release" "ingress" {
  depends_on       = [kubernetes_manifest.lb_addresses]
  for_each         = local.ingresses
  name             = "ingress-nginx-${each.key}"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
  namespace        = "ingress-nginx"
  create_namespace = true
  version          = "4.12.0"
  timeout          = 3600
  set {
    name  = "controller.maxmindLicenseKey"
    value = data.aws_ssm_parameter.params["maxmind_license_key"].value
  }
  set {
    name  = "controller.metrics.serviceMonitor.addionalLabels"
    value = data.aws_ssm_parameter.params["monitoring_release"].value
  }
  values = [<<EOT
nameOverride: nginx-${each.key}
fullNameOverride: nginx-${each.key}
controller:
  extraArgs:
    update-status: "true"
  ingressClassByName: true
  ingressClassResource:
    name: ${each.key}-nginx
    default: ${each.key == "default" ? true : false}
    controllerValue: "k8s.io/${each.key}-ingress-nginx"
  stats:
    enabled: true
  resources:
    requests:
      cpu: 100m
      memory: 100Mi
    limits:
      cpu: 200m
      memory: 200Mi
  metrics:
    enabled: true
    serviceMonitor:
      enabled: true
    prometheusRule:
      enabled: true
      rules: 
        - alert: NGINXConfigFailed
          expr: count(nginx_ingress_controller_config_last_reload_successful == 0) > 0
          for: 1s
          labels:
            severity: critical
          annotations:
            description: bad ingress config - nginx config test failed
            summary: uninstall the latest ingress changes to allow config reloads to resume
        - alert: NGINXCertificateExpiry
          expr: (avg(nginx_ingress_controller_ssl_expire_time_seconds) by (host) - time()) < 604800
          for: 1s
          labels:
            severity: critical
          annotations:
            description: ssl certificate(s) will expire in less then a week
            summary: renew expiring certificates to avoid downtime
        - alert: NGINXTooMany500s
          expr: 100 * ( sum( nginx_ingress_controller_requests{status=~"5.+"} ) / sum(nginx_ingress_controller_requests) ) > 5
          for: 1m
          labels:
            severity: warning
          annotations:
            description: Too many 5XXs
            summary: More than 5% of all requests returned 5XX, this requires your attention
        - alert: NGINXTooMany400s
          expr: 100 * ( sum( nginx_ingress_controller_requests{status=~"4.+"} ) / sum(nginx_ingress_controller_requests) ) > 5
          for: 1m
          labels:
            severity: warning
          annotations:
            description: Too many 4XXs
            summary: More than 5% of all requests returned 4XX, this requires your attention  
  admissionWebhooks:
    enabled: true
    certManager:
      enabled: false
  service:
    loadBalancerIP: ${each.value}
EOT
  ]
}
