locals {
  ssm_params = [
    "dns_zone",
    "route53_aws_access_key_id",
    "route53_aws_secret_access_key",
    "cert_issuer_name",
    "monitoring_release",
    "nas_host",
    "nas_ssh_user",
    "nas_base_dataset",
    "maxmind_license_key",
    "nas_iqn"
  ]
}
data "aws_ssm_parameter" "params" {
  for_each = toset(local.ssm_params)
  name     = "/k8s/${each.key}"
}
data "aws_ssm_parameter" "nas_ssh_key" {
  name = "/cluster/ssh_private_key"
}