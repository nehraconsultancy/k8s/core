resource "helm_release" "nfs" {
  name             = "nfs"
  repository       = "https://democratic-csi.github.io/charts/"
  chart            = "democratic-csi"
  namespace        = "storage"
  create_namespace = true
  version          = "0.14.7"
  timeout          = 3600
  values = [<<EOT
fullnameOverride: nfs
csiDriver:
  name: "org.democratic-csi.nfs"
  attachRequired: false

storageClasses:
- name: nfs
  defaultClass: false
  reclaimPolicy: Retain
  volumeBindingMode: WaitForFirstConsumer
  allowVolumeExpansion: true
  parameters:
    fsType: nfs
  mountOptions:
    - noatime
    - nfsvers=4
  secrets:
    provisioner-secret:
    controller-publish-secret:
    node-stage-secret:
    node-publish-secret:
    controller-expand-secret:

driver:
  config:
    driver: zfs-generic-nfs
    instance_id:
    sshConnection:    
      host: ${data.aws_ssm_parameter.params["nas_host"].value}
      port: 22
      username: ${data.aws_ssm_parameter.params["nas_ssh_user"].value}
    zfs:
      cli:
        sudoEnabled: true
      zvolCompression:
      zvolDedup:
      zvolEnableReservation: false
      zvolBlocksize:
      datasetParentName: ${data.aws_ssm_parameter.params["nas_base_dataset"].value}/nfs/v
      detachedSnapshotsDatasetParentName: ${data.aws_ssm_parameter.params["nas_base_dataset"].value}}/nfs/s
      datasetEnableQuotas: true
      datasetEnableReservation: false
      datasetPermissionsMode: "0777"
      datasetPermissionsUser: root
      datasetPermissionsGroup: wheel
    nfs:
      shareStrategy: "setDatasetProperties"
      shareStrategySetDatasetProperties:
        properties:
          sharenfs: "rw,no_subtree_check,no_root_squash"
          sharenfs: "on"
      shareHost: ${data.aws_ssm_parameter.params["nas_host"].value}
      shareAlldirs: false
      shareAllowedHosts: []
      shareAllowedNetworks: []
      shareMaprootUser: root
      shareMaprootGroup: wheel
      shareMapallUser: ""
      shareMapallGroup: ""
controller:
  tolerations:
    - effect: NoSchedule
      key: node-role.kubernetes.io/control-plane
    - effect: NoSchedule
      key: node-role.kubernetes.io/master
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: node-role.kubernetes.io/control-plane
              operator: "Exists"
EOT
  ]
  set {
    name  = "driver.config.sshConnection.privateKey"
    value = data.aws_ssm_parameter.nas_ssh_key.value
  }
}
