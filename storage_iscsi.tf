resource "helm_release" "iscsi" {
  name             = "iscsi"
  repository       = "https://democratic-csi.github.io/charts/"
  chart            = "democratic-csi"
  namespace        = "storage"
  create_namespace = true
  version          = "0.14.7"
  timeout          = 3600
  values = [<<EOT
fullnameOverride: iscsi
csiDriver:
  name: "org.democratic-csi.iscsi"
  attachRequired: false

storageClasses:
- name: iscsi
  defaultClass: true
  reclaimPolicy: Retain
  volumeBindingMode: WaitForFirstConsumer
  allowVolumeExpansion: true
  parameters:
    fsType: ext4
  mountOptions: []
  secrets:
    provisioner-secret:
    controller-publish-secret:
    node-stage-secret:
    node-publish-secret:
    controller-expand-secret:

driver:
  config:
    driver: zfs-generic-iscsi
    instance_id:
    sshConnection:    
      host: ${data.aws_ssm_parameter.params["nas_host"].value}
      port: 22
      username: ${data.aws_ssm_parameter.params["nas_ssh_user"].value}
    zfs:
      cli:
        sudoEnabled: true
      zvolCompression:
      zvolDedup:
      zvolEnableReservation: false
      zvolBlocksize:
      datasetParentName: ${data.aws_ssm_parameter.params["nas_base_dataset"].value}/iscsi/v
      detachedSnapshotsDatasetParentName: ${data.aws_ssm_parameter.params["nas_base_dataset"].value}}/iscsi/s
    iscsi:
      shareStrategy: "targetCli"
      shareStrategyTargetCli:
        sudoEnabled: true
        basename: ${data.aws_ssm_parameter.params["nas_iqn"].value}
        tpg:
          attributes:
            authentication: 0
            generate_node_acls: 1
            cache_dynamic_acls: 1
            demo_mode_write_protect: 0
          auth:
        block:
          attributes:
            emulate_tpu: 0
      targetPortals: []
      targetPortal: ${data.aws_ssm_parameter.params["nas_host"].value}:3260
      interface:
      namePrefix: csi-
      nameSuffix: "-cluster"
      targetGroups:
        - targetGroupPortalGroup: 1
          targetGroupInitiatorGroup: 1
          targetGroupAuthType: None
          targetGroupAuthGroup:
      extentInsecureTpc: true
      extentXenCompat: false
      extentDisablePhysicalBlocksize: true
      extentBlocksize: 4096
      extentRpm: "SSD"
      extentAvailThreshold: 0

controller:
  tolerations:
    - effect: NoSchedule
      key: node-role.kubernetes.io/control-plane
    - effect: NoSchedule
      key: node-role.kubernetes.io/master
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: node-role.kubernetes.io/control-plane
              operator: "Exists"

node:
  tolerations:
    - effect: NoSchedule
      key: node-role.kubernetes.io/control-plane
    - effect: NoSchedule
      key: node-role.kubernetes.io/master
EOT
  ]
  set {
    name  = "driver.config.sshConnection.privateKey"
    value = data.aws_ssm_parameter.nas_ssh_key.value
  }
}
