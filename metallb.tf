resource "helm_release" "metallb" {
  depends_on       = [helm_release.prometheus_crds]
  name             = "metallb"
  repository       = "https://metallb.github.io/metallb"
  chart            = "metallb"
  namespace        = "metallb-system"
  create_namespace = true
  version          = "0.14.9"
  timeout          = 3600
  values = [<<EOT
prometheus:
  namespace: ${data.aws_ssm_parameter.params["monitoring_release"].value}
  serviceAccount: prometheus
  serviceMonitor:
    enabled: true
  prometheusRule:
    enabled: true
speaker:
  resources:
    limits:
      cpu: 100m
      memory: 100Mi
controller:
  resources:
    limits:
      cpu: 100m
      memory: 100Mi
EOT
  ]
  set {
    name  = "prometheus.namespace"
    value = data.aws_ssm_parameter.params["monitoring_release"].value
  }
  set {
    name  = "prometheus.serviceAccount"
    value = "prometheus"
  }
  set {
    name  = "prometheus.serviceMonitor.enabled"
    value = "true"
  }
  set {
    name  = "prometheus.serviceMonitor.speaker.additionalLabels.release"
    value = data.aws_ssm_parameter.params["monitoring_release"].value
  }
  set {
    name  = "prometheus.serviceMonitor.controller.additionalLabels.release"
    value = data.aws_ssm_parameter.params["monitoring_release"].value
  }
}
