resource "helm_release" "cert_manager" {
  depends_on       = [helm_release.prometheus_crds]
  name             = "certmanager"
  repository       = "https://charts.jetstack.io"
  chart            = "cert-manager"
  namespace        = "cert-manager"
  create_namespace = true
  version          = "v1.16.3"
  timeout          = 3600
  values = [<<EOT
ingressShim:
  defaultIssuerKind: Clusterissuer
  defaultIssuerName: ${data.aws_ssm_parameter.params["cert_issuer_name"].value}-${md5(data.aws_ssm_parameter.params["dns_zone"].value)}
crds:
  enabled: true
prometheus:
  enabled: true
  servicemonitor:
    enabled: true
    labels:
      release: ${data.aws_ssm_parameter.params["monitoring_release"].value}
resources:
  requests:
    cpu: 10m
    memory: 32Mi
  limits:
    cpu: 100m
    memory: 128Mi
EOT
  ]
}
resource "kubernetes_secret_v1" "route53" {
  depends_on = [helm_release.cert_manager]
  metadata {
    name      = "route53"
    namespace = "cert-manager"
  }
  data = {
    access_key_id     = data.aws_ssm_parameter.params["route53_aws_access_key_id"].value
    secret_access_key = data.aws_ssm_parameter.params["route53_aws_secret_access_key"].value
  }
}
