resource "helm_release" "external_dns" {
  name             = "external-dns"
  repository       = "https://kubernetes-sigs.github.io/external-dns/"
  chart            = "external-dns"
  namespace        = "cert-manager"
  create_namespace = true
  version          = "v1.15.0"
  timeout          = 3600
  values = [<<EOT
serviceMonitor:
  enabled: true
fullnameOverride: external-dns
nameOverride: external-dns
provider:
  name: aws
env:
- name: AWS_DEFAULT_REGION
  value: us-east-1
- name: AWS_ACCESS_KEY_ID
  valueFrom:
    secretKeyRef:
      name: route53
      key: access_key_id
- name: AWS_SECRET_ACCESS_KEY
  valueFrom:
    secretKeyRef:
      name: route53
      key: secret_access_key
txtPrefix: ""
txtSuffix: ""
EOT
  ]
}
