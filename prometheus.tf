resource "helm_release" "prometheus_crds" {
  name             = "crds"
  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "prometheus-operator-crds"
  namespace        = data.aws_ssm_parameter.params["monitoring_release"].value
  create_namespace = true
  version          = "17.0.2"
  timeout          = 3600
}
